package cn.xeblog.plugin.tools.browser2.resize;

import cn.hutool.core.util.StrUtil;
import cn.xeblog.plugin.annotation.DoTool;
import cn.xeblog.plugin.persistence.PersistenceService;
import cn.xeblog.plugin.tools.AbstractTool;
import cn.xeblog.plugin.tools.Tools;
import cn.xeblog.plugin.tools.browser.ui.BrowserUI;
import cn.xeblog.plugin.util.NotifyUtils;

import javax.swing.*;
import java.awt.*;

/**
 * @author eleven
 * @date 2024/12/19 10:48
 * @apiNote
 */
@DoTool(Tools.BROWSER2)
public class JxBrowserTool extends AbstractTool {
    private JxBrowserUI browserUI;

    private JPanel mainPanel;

    @Override
    protected void init() {

        String jxBrowserLicense = PersistenceService.getData().getJxBrowserLicense();
        if (StrUtil.isBlank(jxBrowserLicense)) {
            NotifyUtils.error("JxBrowser未配置license",
                    "<html>\n" +
                            "<p>请在插件配置中配置JxBrowserLicense, " +
                            "申请地址<a href='https://teamdev.cn/jxbrowser/release-notes/2022/v7-22/'>https://teamdev.cn/jxbrowser/release-notes/2022/v7-22/</a></p>" +
                            "<p>添加QQ群 754126966 询问也行<p>" +
                            "<p style='color=red'>获取License Key之后在File --> Settings --> Tools --> XEChat --> jxLicense 配置</p>"
            );
            return;
        }

        this.browserUI = new JxBrowserUI(jxBrowserLicense);

        mainPanel= new JPanel();
        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(this.browserUI, BorderLayout.CENTER);
    }

    @Override
    protected JComponent getComponent() {
        return mainPanel;
    }

    @Override
    public void over() {
        super.over();
        if (browserUI != null) {
            this.browserUI.close();
        }
    }
}
