package cn.xeblog.plugin.tools.browser2;

/**
 * @author eleven
 * @date 2024/11/14 10:38
 * @apiNote
 */
public class Const {
    public static final String BROWSER_NAME = "xeChatBrowser";
    public static final String DEFAULT_INDEX = "https://baidu.com";
}
