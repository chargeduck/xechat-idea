package cn.xeblog.plugin.action.handler.command;

import cn.xeblog.plugin.annotation.DoCommand;
import cn.xeblog.plugin.cache.UICache;
import cn.xeblog.plugin.enums.Command;
import cn.xeblog.plugin.ui.ImageUI;

/**
 * @author anlingyi
 * @date 2023/2/18 8:46 PM
 */
@DoCommand(Command.BACKGROUND_IMAGE)
public class BackgroundImageCommandHandler extends AbstractCommandHandler {

    @Override
    protected void process(String[] args) {
        UICache.component = new ImageUI(true);
    }

    @Override
    protected boolean check(String[] args) {
        return true;
    }
}
