package cn.xeblog.plugin.entity;

import lombok.Data;

/**
 * @author eleven
 * @date 2024/11/7 11:06
 * @apiNote
 */
@Data
public class BaiDuDistrict {


    private String code;

    private DataBean data;
    private String ip;

    @Data
    public static class DataBean {
        private String continent;
        private String country;
        private String zipcode;
        private String owner;
        private String isp;
        private String adcode;
        private String prov;
        private String city;
        private String district;

    }
}
