package cn.xeblog.plugin.ui;

import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import lombok.Data;

import javax.swing.*;

/**
 * @author eleven
 * @date 2023/12/14 14:13
 * @apiNote
 */
@Data
public class BackgroundImageUI {
    private JCheckBox checkBox;
    private JSpinner spinner;
    private TextFieldWithBrowseButton folder;
    private JPanel mainPanel;
}
