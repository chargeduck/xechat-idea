package cn.xeblog.plugin.game.dld.model.dto;

import cn.xeblog.plugin.game.dld.model.common.Page;
import lombok.Data;

/**
 * @author eleven
 * @date 2024/1/4 16:45
 * @apiNote
 */
@Data
public class QueryPackageEntryDto {
    private Integer id;

    private Integer playerId;

    private Integer accountId;

    private Integer entryId;

    private String type;

    private Page page;

}
