package cn.xeblog.plugin.game.dld.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author eleven
 * @date 2023/11/29 16:49
 * @apiNote
 */
@Data
@AllArgsConstructor
public class TauntDto {
    private String attackMac;

    private String rivalMac;
}
