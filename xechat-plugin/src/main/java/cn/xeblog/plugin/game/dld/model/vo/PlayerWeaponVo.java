package cn.xeblog.plugin.game.dld.model.vo;

import cn.hutool.core.collection.CollUtil;
import lombok.Data;

import java.util.List;

/**
 * @author eleven
 * @date 2023/12/21 15:07
 * @apiNote
 */
@Data
public class PlayerWeaponVo {
    private String id;

    private String weaponName;

    private Integer grade;

    private Integer addLevel;

    private String intro;

    private Integer weaponType;

    private String iconUrl;

    private Integer sellingPrice;

    private Integer minDamage;

    private Integer maxDamage;

    private String gradeName;

    private String typeName;

    /**
     * 属性描述
     */
    private List<String> thingEntriesDesc;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("名称: ").append(weaponName)
                .append("\n描述: ").append(intro)
                .append("\n攻击: ").append(minDamage).append(" - ").append(maxDamage)
                .append("\n类型: ").append(typeName)
                .append("\n售价: ").append(sellingPrice)
                .append("\n强化等级: ").append(addLevel)
        ;
        if (CollUtil.isNotEmpty(thingEntriesDesc)) {
            sb.append("\n词条:");
            thingEntriesDesc.forEach(item -> sb.append("\n").append(item));
        }
        return sb.toString();

    }
}
