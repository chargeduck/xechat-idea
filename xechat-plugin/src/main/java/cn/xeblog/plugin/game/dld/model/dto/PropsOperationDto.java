package cn.xeblog.plugin.game.dld.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author eleven
 * @date 2024/1/9 8:20
 * @apiNote
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PropsOperationDto {
    /**
     * 背包的id
     */
    private Integer id;

    private Integer playerId;
    /**
     * 使用数量
     */
    private Long number;
}
