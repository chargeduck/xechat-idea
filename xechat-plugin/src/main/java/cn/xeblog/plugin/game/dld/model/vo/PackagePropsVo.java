package cn.xeblog.plugin.game.dld.model.vo;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.stream.StreamUtil;
import cn.hutool.core.util.StrUtil;
import lombok.Data;

/**
 * @author eleven
 * @date 2024/1/5 16:34
 * @apiNote
 */
@Data
public class PackagePropsVo {
    /**
     * 背包id
     */
    private Integer id;
    /**
     * 道具id
     */
    private Integer objId;

    /**
     * 道具名称
     */
    private String propsName;

    /**
     * 道具描述
     */
    private String propsIntro;

    /**
     * 数量
     */
    private Integer num;

    /**
     * 类型 0 锦囊 1礼包 2材料 3技能卡 4宝石 5消耗品
     */
    private Integer type;

    private String typeName;

    /**
     * 售价
     */
    private Integer sellingPrice;

    /**
     * 可以使用
     */
    private Boolean canUse;

    /**
     * 允许打开
     */
    private Boolean allowOpen;

    /**
     * 爆率
     */
    private Double dropProbability;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("名称: ").append(propsName)
                .append("\n类型: ").append(typeName)
                .append("\n爆率: ").append(dropProbability)
                .append("\n售价: ").append(sellingPrice)
                .append("\n数量: ").append(num)
                .append("\n允许打开: ").append(allowOpen)
                .append("\n允许使用: ").append(canUse)
                .append("\n描述：").append(propsIntro)
        ;
        return sb.toString();
    }

    public String getPropsName() {
        if (StrUtil.isBlank(propsName)) {
            return "";
        }
        if (propsName.length() > 6) {
            return propsName.substring(0, 4) + "..";
        }
        return propsName;
    }
}
