package cn.xeblog.plugin.game.dld.model.vo;

import cn.hutool.core.collection.CollUtil;
import lombok.Data;

import java.util.List;

/**
 * @author eleven
 * @date 2024/1/4 17:16
 * @apiNote
 */
@Data
public class PlayerSKillVo {
    /**
     *
     */
    private Integer id;
    /**
     * 账号
     */
    private Integer accountId;
    /**
     * 技能名称
     */
    private String skillName;
    /**
     * 技能描述
     */
    private String skillIntro;
    /**
     * 是否主动技能
     */
    private Boolean activeSkill;
    /**
     * 是否固定伤害
     */
    private Boolean fixedSkill;
    /**
     * 技能等级
     */
    private Integer skillLevel;
    /**
     * 最小伤害
     */
    private Integer minDamage;
    /**
     * 最大伤害
     */
    private Integer maxDamage;
    /**
     * 物品列表
     */
    private List<String> thingEntriesDesc;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("名称: ").append(skillName)
                .append("\n描述: ").append(skillIntro)
                .append("\n攻击: ").append(minDamage).append(" - ").append(maxDamage)
                .append("\n强化等级: ").append(skillLevel)
                .append("\n主动技能:").append(activeSkill)
                .append("\n固伤技能:").append(fixedSkill)

        ;
        if (CollUtil.isNotEmpty(thingEntriesDesc)) {
            sb.append("\n词条:");
            thingEntriesDesc.forEach(item -> sb.append("\n").append(item));
        }
        return sb.toString();
    }
}
