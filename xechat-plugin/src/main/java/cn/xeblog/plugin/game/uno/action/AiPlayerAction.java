package cn.xeblog.plugin.game.uno.action;

import cn.xeblog.plugin.game.uno.entity.PlayerNode;

/**
 * @author eleven
 * @date 2023/4/10 9:58
 * @apiNote
 */
public class AiPlayerAction extends PlayerAction{

    public AiPlayerAction(PlayerNode playerNode) {
       super(playerNode);
    }
}
