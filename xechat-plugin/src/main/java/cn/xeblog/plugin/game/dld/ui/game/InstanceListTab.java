package cn.xeblog.plugin.game.dld.ui.game;

import lombok.Data;

import javax.swing.*;

/**
 * @author eleven
 * @date 2023/10/27 15:39
 * @apiNote
 */
@Data
public class InstanceListTab {
    private JPanel instanceListPanel;
}
