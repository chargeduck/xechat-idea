package cn.xeblog.plugin.game.dld.model.vo;

import lombok.Data;

/**
 * @author eleven
 * @date 2024/1/9 9:31
 * @apiNote
 */
@Data
public class OperaPropsVo {

    private Integer effectRow;

    private String message;
}
