package cn.xeblog.plugin.game.dld.ui.game.playerPackege;

import lombok.Data;

import javax.swing.*;

/**
 * @author eleven
 * @date 2023/11/21 16:13
 * @apiNote
 */
@Data
public class EntryItemTab {
    private JPanel detailPanel;
    private JTextArea detailArea;
    private JPanel propsPanel;
    private JScrollPane propsScroll;
    private JScrollPane detailScroll;
    private JButton discardBtn;
    private JButton sellBtn;
    private JButton enhancedBtn;
    private JButton extBtn;
    private JPanel btnPanel;

}
